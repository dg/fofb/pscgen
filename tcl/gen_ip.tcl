

set ProjectDirPath [file join ${::fwfwk::PrjBuildPath} ${::fwfwk::PrjBuildName}]

## ------------------------------------- ##
## DDS
## ------------------------------------- ##
set ipName "pscgen_dds"

set xcipath [create_ip \
    -name dds_compiler \
    -vendor xilinx.com \
    -library ip -version 6.0 \
    -module_name $ipName]

set_property -dict [list \
    CONFIG.PartsPresent {SIN_COS_LUT_only} \
    CONFIG.Phase_Width $Config(C_W_PHASE) \
    CONFIG.Output_Width $Config(C_W_SINE) \
    CONFIG.Output_Selection {Sine} \
    CONFIG.Has_TREADY {false} \
    CONFIG.S_PHASE_Has_TUSER {User_Field} \
    CONFIG.S_PHASE_TUSER_Width $Config(C_W_TIDX) \
    CONFIG.Parameter_Entry {Hardware_Parameters} \
    CONFIG.Noise_Shaping {None} \
    CONFIG.Has_Phase_Out {false} \
    CONFIG.DATA_Has_TLAST {Not_Required} \
    CONFIG.M_DATA_Has_TUSER {User_Field} \
    CONFIG.M_PHASE_Has_TUSER {Not_Required} \
    CONFIG.Has_ARESETn {true} \
    CONFIG.Has_ACLKEN {false} \
    CONFIG.Latency {6} \
    CONFIG.Output_Frequency1 {0} \
    CONFIG.PINC1 {0} \
    ] [get_ips $ipName]

generate_target all [get_files  ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]
export_ip_user_files -of_objects [get_files ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]


