################################################################################
# Main tcl for the module
################################################################################

# ==============================================================================
proc init {} {
    variable Config
    variable myConf

    # Parse configuration from VHDL package
    parseVhdlConfigFile Config "../hdl/pkg_pscgen.vhd"

    # Configuration
    #set Config(TID_DEPTH) 1024; # This is the minimum

}

# ==============================================================================
proc setSources {} {
  variable Sources

  # Generate VHDL package with modversion
  genModVerFile VHDL ../hdl/pkg_pscgen_version.vhd

  lappend Sources {"../hdl/pkg_pscgen_version.vhd"  "VHDL"}
  lappend Sources {"../hdl/pkg_pscgen.vhd"  "VHDL"}
  lappend Sources {"../hdl/pkg_pscgen_gen_ip.vhd"  "VHDL"}
  lappend Sources {"../hdl/MultiPhaseTable.vhd"  "VHDL"}
  lappend Sources {"../hdl/MultiPhaseAcc.vhd"  "VHDL"}
  lappend Sources {"../hdl/MultiScaler.vhd"  "VHDL"}
  lappend Sources {"../hdl/top_pscgen.vhd"  "VHDL"}
  lappend Sources [list "${::fwfwk::LibPath}/desy_vhdl/hdl/memory/ram/ram_tdp.vhd" "VHDL 2008" "desy"]
  lappend Sources [list "${::fwfwk::LibPath}/desy_vhdl/hdl/math/pkg_math_utils.vhd" "VHDL 2008" "desy"]
  lappend Sources [list "${::fwfwk::LibPath}/desy_vhdl/hdl/math/pkg_math_signed.vhd" "VHDL 2008" "desy"]
  lappend Sources [list "${::fwfwk::LibPath}/desy_vhdl/hdl/common/pkg_common_logic_utils.vhd" "VHDL 2008" "desy"]

  # Simulation sources
  lappend Sources {"../sim/tb_pscgen.vhd"  "VHDL 2008" "" "simulation"}
}

# ==============================================================================
proc setAddressSpace {} {
    variable AddressSpace

    addAddressSpace AddressSpace "pscgen" RDL {} ../rdl/pscgen.rdl

}

# ==============================================================================
proc doOnCreate {} {
  variable Sources
  variable Config

  addSources "Sources"

  # Generate Xilinx IPs
  source "gen_ip.tcl"

}

# ==============================================================================
proc doOnBuild {} {
}

# ==============================================================================
proc setSim {} {

    # On vivado, add prepared waveform
    add_files -fileset sim_1 -norecurse "../sim/pscgen.wcfg"
    #set ::fwfwk::src::SimTop "tb_pscgen"
    #set ::fwfwk::src::SimLibs { }
    #set ::fwfwk::src::SimTime { 10 us }
}
