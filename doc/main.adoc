= Power Supply Controller Generator

|===
| MODULE ID | 0x507E1712 | (1350440722)
|===

// ================================================================================
== Description

This module can generate sine waveform for multiple power supplies.
It takes a stream of (PSCID, value) pair, computes the sine waveform signal and adds it to the value.
It is built to work in association with ComCorr IP.


It is possible to generate up to `2**C_W_TIDX` (default 255) Identifiers.
Each waveform has independents parameters:

* Phase increment
* Phase offset
* Scaling value
* Offset value
* Reset phase

The module generate data values every `TICKER_RATE` clock period.
At each of this tick, the phase of all waveform is incremented and a new wave value is computed for each waveform.
The values are output via the AXI-Stream Output, one value transaction by transfer.

== Overview

The module is a pipeline of blocks that compute the sine signal waveform for each PSCID.

For a given PSCID, the basic scheme is
* A phase accumulator increment each times this PSCID is at the input.
* The increment value is user programmed, a constant phase can be added.
* The DDS convert the phase to a normalized sine signal.
* The sine signal is amplified and offset by user values.

image::overview.png[Overview of the system]

// ================================================================================
== Detailed view

image::detailed.png[Detailed view of the system]

=== Multi Phase Table

This is basically a memory, addressed by the PSCID and retaining user settings for phase increment and phase offset.
Upon reception of a PSCID, it pushes these settings to the pipeline, alongside the PSCID value.

It is possible to source this block with a counter that send burst of PSCID value at a given rate.
This allows generation of value even if there is no input stream, ie for test purpose

=== Multi Phase Acc

This bloc is a set of accumulator.
Upon reception of a PSCID and phase increment & offset, it retrieves the last value of the accumulator, offset it and pushes it to the stream with the PSCID.
This value is incremented and strored again for the next iteration.

=== DDS

A simple sine DDS. Xilinx core.

=== Scaler

This bloc receive the signal value and PSCID.
From the PSCID it lookup for the scaling and offset factor to apply to the signal.

The output of the DDS is multiplied by the scale, which is considered as a fixed point value, containing only decimals.
(The DDS is configured to generate full scale sine values).
The signal offset is then applied, aligned on the fixed point (no decimals).
The final result is scrapped of decimals values.

// ================================================================================
== Runtime configuration

The generator will compute and ouput values from the PSCID at the input.
Entries in the `TABLE_PHASE` and `TABLE_SCALE` for these PSCID shall be configured by the user.
These two tables are addressed by the PSCID number.
Data values pack configuration, as described below.

.TABLE_PHASE_INCR data pack.
|===
| `32-C_W_PHASE` bits | `C_W_PHASE` bits
| unused | Phase increment
|===

.TABLE_PHASE_OFFS data pack.
|===
| `31-*C_W_PHASE` bits |1 bit | `C_W_PHASE` bits
| unused |Reset accumulator | Phase offset
|===

.TABLE_SCALE data pack.
|===
| `32-C_W_OFFSET-C_W_SCALE` bits | `C_W_OFFSET` bits  | `C_W_SCALE` bits
| unused | Signal Offset | Signal Scale Factor
|===

// ================================================================================
== AXIS output format
The generator produces AXIS packets with a compatible format for COMCORR IP module.

The AXIS contains:

* TDATA_VALUE: the sine waveform value (signed integer).
* TDATA_PSCID: the PSCID.
* TUSER: TUSER of input, delayed to stay in phase with data.
* TVALID: data strobe.

The AXIS **does not** contain **TREADY** input on this module.
The COMCORR IP module does not either.

// ================================================================================
== Registers map
include::registers.adoc[]


// ================================================================================
== TODO / Future devs

* The PSCID is now a simple count down from `TABLE_DEPTH` to 0.
  A table to translate from this count to a proper PSCID will be implemented.
  In the meantime, a correct configuration of the COMCORR module shall suffice.
