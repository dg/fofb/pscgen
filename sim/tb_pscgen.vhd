library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library desyrdl;
use desyrdl.common.all;
use desyrdl.pkg_pscgen.all;

use work.pkg_pscgen.all;

entity tb_pscgen is
end entity tb_pscgen;

architecture testbench of tb_pscgen is

    signal tb_clk : std_logic  := '0';
    signal tb_rstn : std_logic := '1';

    signal tb_s_axi_m2s           : t_pscgen_m2s;
    signal tb_s_axi_s2m           : t_pscgen_s2m;
    signal tb_s_axis_tdata_value  : std_logic_vector(C_W_SIG-1 downto 0);
    signal tb_s_axis_tdata_pscid  : std_logic_vector(C_W_TIDX-1 downto 0);
    signal tb_s_axis_tuser        : std_logic_vector(7 downto 0);
    signal tb_s_axis_tvalid       : std_logic;
    signal tb_m_axis_tdata_value  : std_logic_vector(C_W_SIG-1 downto 0);
    signal tb_m_axis_tdata_pscid  : std_logic_vector(C_W_TIDX-1 downto 0);
    signal tb_m_axis_tuser        : std_logic_vector(7 downto 0);
    signal tb_m_axis_tvalid       : std_logic;

    -- AXI4L helpers
    signal tb_s_axi_awtransfer    : std_logic;
    signal tb_s_axi_wtransfer     : std_logic;

    -- Output check helper
    type t_arr_int is array (0 to 2**C_W_TIDX-1) of integer;
    type t_arr_std is array (0 to 2**C_W_TIDX-1) of std_logic_vector(C_W_SIG-1 downto 0);

    signal tb_result : t_arr_int := (others => 0);
    signal tb_result_std : t_arr_std;

begin

    tb_clk <= not tb_clk after 5 ns;

    ---------
    -- DUT --
    ---------
    inst_dut: entity work.top_pscgen
    generic map(
        G_W_TUSER => 8
    )
    port map(
        clk                 => tb_clk,
        rstn                => tb_rstn,
        s_axi_m2s           => tb_s_axi_m2s,
        s_axi_s2m           => tb_s_axi_s2m,
        m_axis_tdata_pscid  => tb_m_axis_tdata_pscid,
        m_axis_tdata_value  => tb_m_axis_tdata_value,
        m_axis_tuser        => tb_m_axis_tuser,
        m_axis_tvalid       => tb_m_axis_tvalid,
        s_axis_tdata_pscid  => tb_s_axis_tdata_pscid,
        s_axis_tdata_value  => tb_s_axis_tdata_value,
        s_axis_tuser        => tb_s_axis_tuser,
        s_axis_tvalid       => tb_s_axis_tvalid
    );

    ------------------
    -- MAIN PROCESS --
    ------------------
    p_main:process

        -- Simple AXI4L write
        procedure write_axi4l(
            address : in std_logic_vector(C_AXI4L_ADDR_WIDTH-1 downto 0);
            data    : in std_logic_vector(C_AXI4L_DATA_WIDTH-1 downto 0)
        ) is
            variable aw_done : boolean := false;
            variable w_done  : boolean := false;
        begin
            tb_s_axi_m2s.awaddr <= address;
            tb_s_axi_m2s.awvalid <= '1';
            tb_s_axi_m2s.wdata <= data;
            tb_s_axi_m2s.wvalid <= '1';

            -- Wait until address and transfer are done
            while not (aw_done and w_done) loop
                wait until rising_edge(tb_clk) and
                (tb_s_axi_awtransfer = '1' or tb_s_axi_wtransfer = '1');

                if tb_s_axi_awtransfer = '1' then
                    aw_done := true;
                    tb_s_axi_m2s.awvalid <= '0';
                end if;

                if tb_s_axi_wtransfer = '1' then
                    w_done := true;
                    tb_s_axi_m2s.wvalid <= '0';
                end if;
            end loop;

        end procedure write_axi4l;

        variable v_phase_incr : std_logic_vector(C_W_PHASE-1 downto 0);
        variable v_phase_offs : std_logic_vector(C_W_PHASE-1 downto 0);
        variable v_scale      : std_logic_vector(C_W_SCALE-1 downto 0);
        variable v_offset     : std_logic_vector(C_W_OFFSET-1 downto 0);
        variable v_axi4l_data : std_logic_vector(C_AXI4L_DATA_WIDTH-1 downto 0);

    begin

        -- reset assertion after some time
        wait for 37 ns;
        tb_rstn                 <= '0';

        -- AXI4L initial values
        tb_s_axi_m2s.awvalid    <= '0';
        tb_s_axi_m2s.wvalid     <= '0';
        tb_s_axi_m2s.arvalid    <= '0';

        -- AXI4L default/constant values
        tb_s_axi_m2s.awprot     <= (others => '0');
        tb_s_axi_m2s.arprot     <= (others => '0');
        tb_s_axi_m2s.wstrb      <= (others => '1');
        tb_s_axi_m2s.bready     <= '1';
        tb_s_axi_m2s.rready     <= '1';

        tb_s_axis_tvalid       <= '0';

        wait for 40 ns;
        wait until rising_edge(tb_clk);
        tb_rstn <= '1';
        wait for 10 ns;
        wait until rising_edge(tb_clk);

        -- Write config throught axi4
        write_axi4l(std_logic_vector(C_REGISTER_INFO(C_TICKER_RATE_ID).address),
                    std_logic_vector(to_unsigned(100, C_AXI4L_DATA_WIDTH)));
        write_axi4l(std_logic_vector(C_REGISTER_INFO(C_TABLE_DEPTH_ID).address),
                    std_logic_vector(to_unsigned(6, C_AXI4L_DATA_WIDTH)));

        -- Phase table parameters
        for I in 0 to 6 loop
            v_phase_incr := std_logic_vector(to_unsigned(1300*(I+1), C_W_PHASE));
            v_phase_offs := std_logic_vector(to_unsigned(3300*2*I, C_W_PHASE));

            v_axi4l_data := (others => '0');
            v_axi4l_data(C_W_PHASE-1 downto 0)  := v_phase_incr;
            write_axi4l(std_logic_vector(C_MEM_INFO(0).address+to_unsigned(I*4, C_AXI4L_ADDR_WIDTH)),
                        v_axi4l_data);

            v_axi4l_data := (others => '0');
            v_axi4l_data(C_W_PHASE-1 downto 0)  := v_phase_offs;
            v_axi4l_data(C_W_PHASE)             := '1'; -- At first reset all
            write_axi4l(std_logic_vector(C_MEM_INFO(1).address+to_unsigned(I*4, C_AXI4L_ADDR_WIDTH)),
                        v_axi4l_data);
        end loop;

        -- Scaler table parameters
        for I in 0 to 6 loop
            v_scale  := std_logic_vector(to_unsigned(2048+I*4096, C_W_SCALE));
            v_offset := std_logic_vector(to_signed((6-I)*2096, C_W_OFFSET));
            v_axi4l_data := (others => '0');
            v_axi4l_data(C_W_SCALE-1 downto 0)                      := v_scale;
            v_axi4l_data(C_W_OFFSET+C_W_SCALE-1 downto C_W_SCALE)   := v_offset;
            write_axi4l(std_logic_vector(C_MEM_INFO(2).address+to_unsigned(I*4, C_AXI4L_ADDR_WIDTH)),
                        v_axi4l_data);
        end loop;


        -- Start the phase accu
        v_axi4l_data := (0=> '1', others => '0');
        write_axi4l(std_logic_vector(C_REGISTER_INFO(C_CONTROL_ID).address),
                    v_axi4l_data);

        -- Wait a few iterations, for the accu table to reset
        wait for 3 us;
        wait until rising_edge(tb_clk);

        -- Phase table parameters
        for I in 0 to 6 loop
            v_phase_offs := std_logic_vector(to_unsigned(33*2*I, C_W_PHASE));

            v_axi4l_data := (others => '0');
            v_axi4l_data(C_W_PHASE-1 downto 0)  := v_phase_offs;
            v_axi4l_data(C_W_PHASE)             := '0';
            write_axi4l(std_logic_vector(C_MEM_INFO(1).address+to_unsigned(I*4, C_AXI4L_ADDR_WIDTH)),
                        v_axi4l_data);

        end loop;

        wait for 3 us;
        wait until rising_edge(tb_clk);

        -- ====================================================================================================
        -- Test with INPUT
        v_axi4l_data := (1=> '1', others => '0');
        write_axi4l(std_logic_vector(C_REGISTER_INFO(C_CONTROL_ID).address),
                    v_axi4l_data);

        -- Scaler table parameters
        for I in 0 to 7 loop
            v_scale  := std_logic_vector(to_unsigned(0, C_W_SCALE));
            v_offset := std_logic_vector(to_signed(I, C_W_OFFSET));
            v_axi4l_data := (others => '0');
            v_axi4l_data(C_W_SCALE-1 downto 0)                      := v_scale;
            v_axi4l_data(C_W_OFFSET+C_W_SCALE-1 downto C_W_SCALE)   := v_offset;
            write_axi4l(std_logic_vector(C_MEM_INFO(2).address+to_unsigned(I*4, C_AXI4L_ADDR_WIDTH)),
                        v_axi4l_data);
        end loop;

        v_offset := std_logic_vector(to_signed(66, C_W_OFFSET));
        v_axi4l_data(C_W_OFFSET+C_W_SCALE-1 downto C_W_SCALE)   := v_offset;
        write_axi4l(std_logic_vector(C_MEM_INFO(2).address+to_unsigned(20*4, C_AXI4L_ADDR_WIDTH)),
                        v_axi4l_data);

        -- Phase table parameters
        for I in 0 to 6 loop
            v_phase_offs := std_logic_vector(to_unsigned(0, C_W_PHASE));

            v_axi4l_data := (others => '0');
            v_axi4l_data(C_W_PHASE-1 downto 0)  := v_phase_offs;
            v_axi4l_data(C_W_PHASE)             := '1'; -- At first reset all
            write_axi4l(std_logic_vector(C_MEM_INFO(1).address+to_unsigned(I*4, C_AXI4L_ADDR_WIDTH)),
                        v_axi4l_data);
        end loop;

        wait for 1 us;
        wait until rising_edge(tb_clk);

        for I in 0 to 6 loop
            for J in 1 to 20 loop
                tb_s_axis_tdata_value  <= std_logic_vector(to_signed(0, C_W_PHASE));
                --tb_s_axis_tdata_value  <= std_logic_vector(to_signed(I*200 + J, C_W_PHASE));
                tb_s_axis_tdata_pscid  <= std_logic_vector(to_unsigned(J, C_W_TIDX));
                tb_s_axis_tuser        <= std_logic_vector(to_unsigned(100+J, 8));
                tb_s_axis_tvalid       <= '1';
                wait until rising_edge(tb_clk);
            end loop;

            tb_s_axis_tvalid       <= '0';

            wait for 1 us;
            wait until rising_edge(tb_clk);
        end loop;


        -- infinite wait at the end
        wait;

    end process p_main;


    -------------------
    -- AXI4L HELPERS --
    -------------------
    tb_s_axi_awtransfer    <= tb_s_axi_m2s.awvalid and tb_s_axi_s2m.awready;
    tb_s_axi_wtransfer     <= tb_s_axi_m2s.wvalid and tb_s_axi_s2m.wready;

    --------------------
    -- AXI4 STREAM RX --
    --------------------
    p_axis_rx:process(tb_clk, tb_rstn)
        variable pscid : natural;
    begin
        if tb_rstn = '0' then
            tb_result <= (others => 0);
            tb_result_std <= (others => (others=>'0'));
        elsif rising_edge(tb_clk) then
            if tb_m_axis_tvalid = '1' then
                pscid := to_integer(unsigned(tb_m_axis_tdata_pscid));
                tb_result_std(pscid) <= tb_m_axis_tdata_value;
                tb_result(pscid) <= to_integer(signed(tb_m_axis_tdata_value));
            end if;
        end if;
    end process p_axis_rx;


end architecture testbench;

