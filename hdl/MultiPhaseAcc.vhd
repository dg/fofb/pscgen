library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library desy;
use desy.ram_tdp;

use work.pkg_pscgen.all;

entity MultiPhaseAcc is
    port(
        clk                    : in std_logic;
        rstn                   : in std_logic;

        -- phase in
        pi_phase_incr          : in std_logic_vector(C_W_PHASE-1 downto 0);
        pi_phase_offset        : in std_logic_vector(C_W_PHASE-1 downto 0);
        pi_phase_reset         : in std_logic;
        pi_phase_tidx          : in std_logic_vector(C_W_TIDX-1 downto 0);
        pi_phase_valid         : in std_logic;

        -- phase out
        po_phase               : out std_logic_vector(C_W_PHASE-1 downto 0);
        po_phase_tidx          : out std_logic_vector(C_W_TIDX-1 downto 0);
        po_phase_valid         : out std_logic

    );
end entity MultiPhaseAcc;

architecture rtl of MultiPhaseAcc is

    signal accu_prev      : std_logic_vector(C_W_PHASE-1 downto 0);
    signal accu_next      : std_logic_vector(C_W_PHASE-1 downto 0);

    signal r_accu_valid   : std_logic_vector(1 downto 0);
    signal r_accu_incr    : std_logic_vector(C_W_PHASE-1 downto 0);
    signal r_accu_reset   : std_logic;
    signal r_accu_tidx    : std_logic_vector(C_W_TIDX-1 downto 0);
    signal r_phase_offset : std_logic_vector(C_W_PHASE-1 downto 0);

    signal phase          : std_logic_vector(C_W_PHASE-1 downto 0);
    signal phase_tidx     : std_logic_vector(C_W_TIDX-1 downto 0);

begin


    ----------------
    -- PIPE DELAY --
    ----------------
    p_accu:process(clk, rstn)
    begin
        if rstn = '0' then
            r_accu_reset   <= '0';
            r_accu_valid   <= (others => '0');
            r_accu_tidx    <= (others => '0');
            r_accu_incr    <= (others => '0');
            r_phase_offset <= (others => '0');

        elsif rising_edge(clk) then

            -- Pipe delay on valid
            r_accu_valid   <= r_accu_valid(r_accu_valid'left-1 downto 0) & pi_phase_valid;
            r_accu_tidx    <= pi_phase_tidx;
            r_accu_incr    <= pi_phase_incr;
            r_accu_reset   <= pi_phase_reset;
            r_phase_offset <= pi_phase_offset;

        end if;
    end process;

    ---------------------
    -- ACCUMULATOR SUM --
    ---------------------
    accu_next <= (others => '0') when r_accu_reset = '1' else
                 std_logic_vector(unsigned(r_accu_incr) + unsigned(accu_prev));

    -----------------------
    -- ACCUMULATOR TABLE --
    -----------------------
    -- Port A read the last value
    -- Port B write the next
    inst_accu_table: entity desy.ram_tdp
    generic map(
        G_ADDR  => C_W_TIDX,
        G_DATA  => C_W_PHASE
    )
    port map(
        pi_clk_a    => clk,
        pi_en_a     => '1',
        pi_we_a     => '0',
        pi_addr_a   => pi_phase_tidx,
        pi_data_a   => (others => '0'),
        po_data_a   => accu_prev,
        pi_clk_b    => clk,
        pi_en_b     => '1',
        pi_we_b     => r_accu_valid(0),
        pi_addr_b   => r_accu_tidx,
        pi_data_b   => accu_next,
        po_data_b   => open
    );

    ------------------
    -- PHASE OFFSET --
    ------------------
    p_offset:process(clk, rstn)
    begin
        if rstn = '0' then
            phase      <= (others => '0');
            phase_tidx <= (others => '0');
        elsif rising_edge(clk) then
            phase      <= std_logic_vector(unsigned(accu_prev) + unsigned(r_phase_offset));
            phase_tidx <= r_accu_tidx;
        end if;
    end process;

    ------------
    -- OUTPUT --
    ------------
    po_phase       <= phase;
    po_phase_tidx  <= phase_tidx;
    po_phase_valid <= r_accu_valid(1);

end architecture rtl;
