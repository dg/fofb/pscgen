
library ieee;
use ieee.math_real.ceil;
use ieee.std_logic_1164.all;

use work.pkg_pscgen.all;

package pkg_pscgen_gen_ip is

    -- DDS TDATA is extended to the next byte boundary
    constant C_W_EXT_SINE : natural := natural(ceil(real(C_W_SINE)/8.0)*8.0);
    constant C_W_EXT_PHASE : natural := natural(ceil(real(C_W_PHASE)/8.0)*8.0);

    COMPONENT pscgen_dds
      PORT (
        aclk : IN STD_LOGIC;
        aresetn : IN STD_LOGIC;
        s_axis_phase_tvalid : IN STD_LOGIC;
        s_axis_phase_tdata : IN STD_LOGIC_VECTOR(C_W_EXT_PHASE-1 DOWNTO 0);
        s_axis_phase_tuser : IN STD_LOGIC_VECTOR(C_W_TIDX-1 DOWNTO 0);
        m_axis_data_tvalid : OUT STD_LOGIC;
        m_axis_data_tdata : OUT STD_LOGIC_VECTOR(C_W_EXT_SINE-1 DOWNTO 0);
        m_axis_data_tuser : OUT STD_LOGIC_VECTOR(C_W_TIDX-1 DOWNTO 0)
      );
    END COMPONENT;


end package;
