library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library desy;
use desy.ram_tdp;

use work.pkg_pscgen.all;
use work.pkg_pscgen_gen_ip.all;

entity MultiScaler is
    port(
        clk                    : in std_logic;
        rstn                   : in std_logic;

        -- Memory map to table
        pi_table_en            : in std_logic;
        pi_table_we            : in std_logic;
        pi_table_addr          : in std_logic_vector(C_W_TIDX-1 downto 0);
        pi_table_data          : in std_logic_vector(C_W_SCALE+C_W_OFFSET-1 downto 0);
        po_table_data          : out std_logic_vector(C_W_SCALE+C_W_OFFSET-1 downto 0);

        -- Sine in
        pi_sine                : in std_logic_vector(C_W_SINE-1 downto 0);
        pi_sine_tidx           : in std_logic_vector(C_W_TIDX-1 downto 0);
        pi_sine_valid          : in std_logic;

        -- Wave out
        po_wave                : out std_logic_vector(C_W_SIG-1 downto 0);
        po_wave_tidx           : out std_logic_vector(C_W_TIDX-1 downto 0);
        po_wave_valid          : out std_logic
    );
end entity MultiScaler;

architecture rtl of MultiScaler is


    -- CONSTANT
    constant C_PIPE_LEN     : natural   := 3;

    -- Array for TIDX pipeline delay
    type t_tidx_array is array(C_PIPE_LEN-1 downto 0) of std_logic_vector(C_W_TIDX-1 downto 0);

    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------
    signal r_valid    : std_logic_vector(C_PIPE_LEN-1 downto 0);
    signal r_sine     : std_logic_vector(C_W_SINE-1 downto 0);
    signal r_tidx     : t_tidx_array;
    signal r_offset   : std_logic_vector(C_W_OFFSET-1 downto 0);

    signal table_data : std_logic_vector(C_W_SCALE+C_W_OFFSET-1 downto 0);
    signal scale      : std_logic_vector(C_W_SCALE-1 downto 0);
    signal offset     : std_logic_vector(C_W_OFFSET-1 downto 0);

    signal mult       : signed(C_W_SCALE+C_W_SINE-1 downto 0);
    signal pad_offset : std_logic_vector(C_W_OFFSET+C_W_PADDING-1 downto 0);
    signal madd       : signed(C_W_SCALE+C_W_SINE-1 downto 0);

    signal wave       : std_logic_vector(C_W_SIG-1 downto 0);

begin


    --------------------
    -- PIPELINE DELAY --
    --------------------
    p_delay:process(clk, rstn)
    begin
        if rstn = '0' then
            r_valid                         <= (others => '0');
            r_sine                          <= (others => '0');
            r_tidx                          <= (others => (others => '0'));
            r_offset                        <= (others => '0');
        elsif rising_edge(clk) then
            r_valid                         <= pi_sine_valid & r_valid(C_PIPE_LEN-1 downto 1);
            r_sine                          <= pi_sine;
            r_offset                        <= offset;

            r_tidx(C_PIPE_LEN-1)            <= (pi_sine_tidx);
            for I in 0 to C_PIPE_LEN-2 loop
                r_tidx(I)                   <= r_tidx(I+1);
            end loop;
        end if;
    end process;

    ------------------------
    -- SCALE OFFSET TABLE --
    ------------------------
    -- Port A is read write from AXI controller, Port B is read only from logic
    inst_phase_table: entity desy.ram_tdp
    generic map(
        G_ADDR  => C_W_TIDX,
        G_DATA  => C_W_SCALE+C_W_OFFSET
    )
    port map(
        pi_clk_a    => clk,
        pi_en_a     => pi_table_en,
        pi_we_a     => pi_table_we,
        pi_addr_a   => pi_table_addr,
        pi_data_a   => pi_table_data,
        po_data_a   => po_table_data,
        pi_clk_b    => clk,
        pi_en_b     => '1',
        pi_we_b     => '0',
        pi_addr_b   => std_logic_vector(pi_sine_tidx),
        pi_data_b   => (others => '0'),
        po_data_b   => table_data
    );

    -- Unpack table data
    scale   <= table_data(C_W_SCALE-1 downto 0);
    offset  <= table_data(C_W_SCALE+C_W_OFFSET-1 downto C_W_SCALE);

    ---------------------
    -- MULTPLY AND ADD --
    ---------------------
    pad_offset(C_W_OFFSET+C_W_PADDING-1 downto C_W_PADDING) <= r_offset;
    pad_offset(C_W_PADDING-1 downto 0) <= (others => '0');

    p_multadd:process(clk, rstn)
    begin
        if rstn = '0' then
            mult <= (others => '0');
            madd <= (others => '0');
        elsif rising_edge(clk) then
            mult <= signed(r_sine) * signed(scale);
            madd <= mult + signed(pad_offset);
        end if;
    end process;

    wave <= std_logic_vector(madd(C_W_SINE+C_W_SCALE-1 downto C_W_PADDING));

    --------------
    -- PORT OUT --
    --------------
    po_wave       <= wave;
    po_wave_tidx  <= r_tidx(0);
    po_wave_valid <= r_valid(0);

end architecture;

