package pkg_pscgen is

    constant C_W_PHASE   : natural := 16;
    constant C_W_VALUE   : natural := 16;
    constant C_W_PSCID   : natural := 8;
    constant C_W_TIDX    : natural := 8;
    constant C_W_SINE    : natural := 26;
    constant C_W_SIG     : natural := 16;
    constant C_W_SCALE   : natural := 16;
    constant C_W_OFFSET  : natural := 16;


    -- Will not be parsed by tcl, sadly
    constant C_W_PADDING    : natural   := C_W_SINE+C_W_SCALE-C_W_SIG;

end package pkg_pscgen;
