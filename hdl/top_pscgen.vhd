library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library desyrdl;
use desyrdl.common.all;
use desyrdl.pkg_pscgen.all;

library desy;
use desy.math_signed.f_sum_sat;

use work.pkg_pscgen.all;
use work.pkg_pscgen_version.all;
use work.pkg_pscgen_gen_ip.all;

entity top_pscgen is
    generic(
        G_W_TUSER           : natural := 8
    );
    port(
        clk                 : in std_logic;
        rstn                : in std_logic;

        -- AXI MM interface
        s_axi_m2s           : in t_pscgen_m2s;
        s_axi_s2m           : out t_pscgen_s2m;

        -- AXIS input
        s_axis_tdata_pscid  : in std_logic_vector(C_W_PSCID-1 downto 0);
        s_axis_tdata_value  : in std_logic_vector(C_W_VALUE-1 downto 0);
        s_axis_tuser        : in std_logic_vector(G_W_TUSER-1 downto 0);
        s_axis_tvalid       : in std_logic;

        -- AXIS output
        m_axis_tdata_pscid  : out std_logic_vector(C_W_PSCID-1 downto 0);
        m_axis_tdata_value  : out std_logic_vector(C_W_VALUE-1 downto 0);
        m_axis_tuser        : out std_logic_vector(G_W_TUSER-1 downto 0);
        m_axis_tvalid       : out std_logic
    );
end entity top_pscgen;

architecture struct of top_pscgen is

    constant C_DELAY        : natural := 12;
    type aslv is array(0 to C_DELAY-1) of std_logic_vector(G_W_TUSER+C_W_VALUE-1 downto 0);

    signal rst              : std_logic;

    signal addr_i           : t_addrmap_pscgen_in;
    signal addr_o           : t_addrmap_pscgen_out;


    signal phase_incr       : std_logic_vector(C_W_PHASE-1 downto 0);
    signal phase_offset     : std_logic_vector(C_W_PHASE-1 downto 0);
    signal phase_reset      : std_logic;
    signal phase_incr_tidx  : std_logic_vector(C_W_TIDX-1 downto 0);
    signal phase_incr_valid : std_logic;

    signal phase            : std_logic_vector(C_W_PHASE-1 downto 0);
    signal phase_tidx       : std_logic_vector(C_W_TIDX-1 downto 0);
    signal phase_valid      : std_logic;

    signal sine             : std_logic_vector(C_W_EXT_SINE-1 downto 0);
    signal sine_tidx        : std_logic_vector(C_W_TIDX-1 downto 0);
    signal sine_valid       : std_logic;

    signal wave             : std_logic_vector(C_W_SIG-1 downto 0);
    signal wave_tidx        : std_logic_vector(C_W_TIDX-1 downto 0);
    signal wave_valid       : std_logic;

    signal r_dds_tvalid     : std_logic_vector(5 downto 0);

    signal data_delay       : aslv;

    signal seq_inc_out_s    : std_logic_vector(7 downto 0);
    signal ticker_enable_s  : std_logic;

begin

    rst <= not rstn;


    ------------
    -- AXI MM --
    ------------
    inst_aximm:entity desyrdl.pscgen
    port map(
        pi_clock => clk,
        pi_reset => rst,
        pi_s_top  => s_axi_m2s,
        po_s_top  => s_axi_s2m,
        pi_addrmap => addr_i,
        po_addrmap => addr_o
    );

    addr_i.version.data.data    <= C_VERSION;

    -----------------
    -- PHASE TABLE --
    -----------------
    inst_phase_table:entity work.MultiPhaseTable
    port map(
        clk                    => clk,
        rstn                   => rstn,

        -- Memory map to table
        pi_table_incr_en            => addr_o.table_phase_incr.en,
        pi_table_incr_we            => addr_o.table_phase_incr.we,
        pi_table_incr_addr          => addr_o.table_phase_incr.addr(C_W_TIDX-1 downto 0),
        pi_table_incr_data          => addr_o.table_phase_incr.data(C_W_PHASE-1 downto 0),
        po_table_incr_data          => addr_i.table_phase_incr.data(C_W_PHASE-1 downto 0),
        pi_table_offs_en            => addr_o.table_phase_offs.en,
        pi_table_offs_we            => addr_o.table_phase_offs.we,
        pi_table_offs_addr          => addr_o.table_phase_offs.addr(C_W_TIDX-1 downto 0),
        pi_table_offs_data          => addr_o.table_phase_offs.data(C_W_PHASE downto 0),
        po_table_offs_data          => addr_i.table_phase_offs.data(C_W_PHASE downto 0),

        -- Configuration
        ticker_enable          => addr_o.control.enable_ticker.data(0),
        input_enable           => addr_o.control.enable_input.data(0),
        ticker_rate            => addr_o.ticker_rate.data.data,
        table_depth            => addr_o.table_depth.data.data,

        pi_phase_tidx          => s_axis_tdata_pscid,
        pi_phase_valid         => s_axis_tvalid,

        po_phase_incr          => phase_incr,
        po_phase_offset        => phase_offset,
        po_phase_reset         => phase_reset,
        po_phase_tidx          => phase_incr_tidx,
        po_phase_valid         => phase_incr_valid,

        seq_inc_out            => seq_inc_out_s

    );

    addr_i.table_phase_incr.data(31 downto C_W_PHASE)   <= (others => '0');
    addr_i.table_phase_offs.data(31 downto C_W_PHASE+1) <= (others => '0');
    ticker_enable_s <= addr_o.addr_o.control.enable_ticker.data(0);
    ----------------
    -- PHASE ACCU --
    ----------------

    inst_phase_accu: entity work.MultiPhaseAcc
    port map(
        clk             => clk,
        rstn            => rstn,

        -- phase in
        pi_phase_incr   => phase_incr,
        pi_phase_offset => phase_offset,
        pi_phase_reset  => phase_reset,
        pi_phase_tidx   => phase_incr_tidx,
        pi_phase_valid  => phase_incr_valid,

        -- phase out
        po_phase        => phase,
        po_phase_tidx   => phase_tidx,
        po_phase_valid  => phase_valid
    );

    ---------
    -- DDS --
    ---------
    inst_dds: pscgen_dds
    port map (
        aclk                                                 => clk,
        aresetn                                              => rstn,
        s_axis_phase_tvalid => '1', -- otherwise DDS get stuck...
        s_axis_phase_tdata(C_W_PHASE-1 downto 0)             => phase,
        s_axis_phase_tuser                                   => phase_tidx,
        m_axis_data_tvalid                                   => sine_valid,
        m_axis_data_tdata                                    => sine,
        m_axis_data_tuser                                    => sine_tidx
    );

    -- Manual pipe delay of tvalid, otherwise DDS is stuck...
    p_pipedelay:process(clk, rstn)
    begin
        if rstn='0' then
            r_dds_tvalid <= (others => '0');
        elsif rising_edge(clk) then
            r_dds_tvalid <= phase_valid & r_dds_tvalid(r_dds_tvalid'left downto 1);
        end if;
    end process;


    ------------
    -- SCALER --
    ------------
    inst_scaler: entity work.MultiScaler
    port map(
        clk                    => clk,
        rstn                   => rstn,

        -- Memory map to table
        pi_table_en            => addr_o.table_scale.en,
        pi_table_we            => addr_o.table_scale.we,
        pi_table_addr          => addr_o.table_scale.addr(C_W_TIDX-1 downto 0),
        pi_table_data          => addr_o.table_scale.data(C_W_SCALE+C_W_OFFSET-1 downto 0),
        po_table_data          => addr_i.table_scale.data(C_W_SCALE+C_W_OFFSET-1 downto 0),

        -- Sine in
        pi_sine                => sine(C_W_SINE-1 downto 0),
        pi_sine_tidx           => sine_tidx,
        pi_sine_valid          => r_dds_tvalid(0),

        -- Wave out
        po_wave                => wave,
        po_wave_tidx           => wave_tidx,
        po_wave_valid          => wave_valid
        
    );

    gen_fixmsb:if C_W_SCALE+C_W_OFFSET < 32 generate
        addr_i.table_scale.data(31 downto C_W_SCALE+C_W_OFFSET-1) <= (others => '0');
    end generate;


    ----------------------
    -- INPUT DATA DELAY --
    ----------------------
    p_data_delay:process(clk, rstn)
    begin
        if rstn = '0' then
            data_delay <= (others => (others => '0'));
        elsif rising_edge(clk) then
            data_delay(0) <= s_axis_tuser & s_axis_tdata_value;
            data_delay(1 to C_DELAY-1) <= data_delay(0 to C_DELAY-2);
        end if;
    end process;

    ------------------
    -- OUTPUT ADDER --
    ------------------

    p_output:process(clk, rstn)
    begin
        if rstn = '0' then
            m_axis_tdata_pscid  <= (others => '0');
            m_axis_tdata_value  <= (others => '0');
            m_axis_tuser        <= (others => '0');
            m_axis_tvalid       <= '0';
        elsif rising_edge(clk) then
            m_axis_tdata_pscid          <= wave_tidx;

            m_axis_tdata_value          <= std_logic_vector(f_sum_sat(
                                            signed(data_delay(C_DELAY-1)(C_W_VALUE-1 downto 0)),
                                            signed(wave)
                                        ));

            
            if ticker_enable_s = '1' then
                m_axis_tuser <= seq_inc_out_s;
            else
                m_axis_tuser  <= data_delay(C_DELAY-1)(G_W_TUSER+C_W_VALUE-1 downto C_W_VALUE);
            end if;

            m_axis_tvalid <= wave_valid;
        end if;
    end process;


end architecture struct;
