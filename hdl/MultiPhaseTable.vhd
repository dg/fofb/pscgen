library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library desy;
use desy.ram_tdp;

use work.pkg_pscgen.all;

entity MultiPhaseTable is
    port(
        clk     : in std_logic;
        rstn    : in std_logic;

        -- Memory map to table
        pi_table_incr_en         : in std_logic;
        pi_table_incr_we         : in std_logic;
        pi_table_incr_addr       : in std_logic_vector(C_W_TIDX-1 downto 0);
        pi_table_incr_data       : in std_logic_vector(C_W_PHASE-1 downto 0);
        po_table_incr_data       : out std_logic_vector(C_W_PHASE-1 downto 0);
        pi_table_offs_en         : in std_logic;
        pi_table_offs_we         : in std_logic;
        pi_table_offs_addr       : in std_logic_vector(C_W_TIDX-1 downto 0);
        pi_table_offs_data       : in std_logic_vector(C_W_PHASE downto 0);
        po_table_offs_data       : out std_logic_vector(C_W_PHASE downto 0);

        -- Configuration
        ticker_enable       : in std_logic;
        input_enable        : in std_logic;
        ticker_rate         : in std_logic_vector(31 downto 0);
        table_depth         : in std_logic_vector(C_W_TIDX-1 downto 0);

        -- TIDX input
        pi_phase_tidx          : in std_logic_vector(C_W_TIDX-1 downto 0);
        pi_phase_valid         : in std_logic;

        po_phase_incr          : out std_logic_vector(C_W_PHASE-1 downto 0);
        po_phase_offset        : out std_logic_vector(C_W_PHASE-1 downto 0);
        po_phase_reset         : out std_logic;
        po_phase_tidx          : out std_logic_vector(C_W_TIDX-1 downto 0);
        po_phase_valid         : out std_logic;

        seq_inc_out            : out std_logic_vector(7 downto 0)

    );
end entity MultiPhaseTable;

architecture rtl of MultiPhaseTable is

    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------
    signal ticker_cnt      : unsigned(31 downto 0);
    signal ticker_tick     : std_logic;

    signal ticker_tidx            : std_logic_vector(C_W_TIDX-1 downto 0);
    signal ticker_tidx_cnt        : unsigned(C_W_TIDX-1 downto 0);
    signal ticker_tidx_valid      : std_logic;

    signal tidx            : std_logic_vector(C_W_TIDX-1 downto 0);
    signal tidx_valid      : std_logic;

    signal r_phase_tidx    : std_logic_vector(C_W_TIDX-1 downto 0);
    signal r_phase_valid   : std_logic;

    signal table_incr_data      : std_logic_vector(C_W_PHASE-1 downto 0);
    signal table_offs_data      : std_logic_vector(C_W_PHASE downto 0);

    signal seq_inc_out_s        : std_logic_vector(7 downto 0);
begin


    ---------------------------
    -- TICKER RATE DECOUNTER --
    ---------------------------
    seq_inc_out <= seq_inc_out_s;
    p_ticker:process(clk, rstn)
    begin
        if rstn = '0' then
            ticker_cnt  <= (others => '0');
            ticker_tick <= '0';
        elsif rising_edge(clk) then
            if ticker_cnt = 0 then
                ticker_cnt <= unsigned(ticker_rate);
                ticker_tick <= ticker_enable;
                if seq_inc_out_s = 255 then 
                    seq_inc_out_s <= (others => '0');
                else 
                    seq_inc_out_s <= seq_inc_out_s + 1;
                end if;
            else
                ticker_cnt <= ticker_cnt-1;
                ticker_tick <= '0';

            end if;
        end if;
    end process p_ticker;

    --------------------------------
    -- TICKER TABLE IDX DECOUNTER --
    --------------------------------
    p_ticker_tidx:process(clk, rstn)
    begin
        if rstn = '0' then
            ticker_tidx_cnt        <= (others => '0');
            ticker_tidx_valid      <= '0';
        elsif rising_edge(clk) then

            -- NID de-counter
            if ticker_tidx_cnt = 0 then
                if ticker_tick = '1' then
                    ticker_tidx_cnt    <= unsigned(table_depth);
                    ticker_tidx_valid  <= '1';
                else
                    ticker_tidx_cnt    <= (others => '0');
                    ticker_tidx_valid  <= '0';
                end if;
            else
                ticker_tidx_cnt        <= ticker_tidx_cnt-1;
                ticker_tidx_valid      <= '1';
            end if;
        end if;
    end process p_ticker_tidx;

    tidx <= std_logic_vector(ticker_tidx_cnt) when ticker_enable = '1' else
            pi_phase_tidx when input_enable = '1' else
            (others => '0');

    tidx_valid <= ticker_tidx_valid when ticker_enable = '1' else
                  pi_phase_valid when input_enable = '1' else
                  '0';


    ----------------------
    -- PHASE INCR TABLE --
    ----------------------
    -- Port A is read write from AXI controller, Port B is read only from logic
    inst_incr_table: entity desy.ram_tdp
    generic map(
        G_ADDR  => C_W_TIDX,
        G_DATA  => C_W_PHASE
    )
    port map(
        pi_clk_a    => clk,
        pi_en_a     => pi_table_incr_en,
        pi_we_a     => pi_table_incr_we,
        pi_addr_a   => pi_table_incr_addr,
        pi_data_a   => pi_table_incr_data,
        po_data_a   => po_table_incr_data,
        pi_clk_b    => clk,
        pi_en_b     => '1',
        pi_we_b     => '0',
        pi_addr_b   => std_logic_vector(tidx),
        pi_data_b   => (others => '0'),
        po_data_b   => table_incr_data
    );
    -- Unpack phase data
    po_phase_incr   <= table_incr_data(C_W_PHASE-1 downto 0);
    po_phase_valid  <= r_phase_valid;
    po_phase_tidx   <= r_phase_tidx;

    ----------------------
    -- PHASE OFFS TABLE --
    ----------------------
    -- Port A is read write from AXI controller, Port B is read only from logic
    inst_offs_table: entity desy.ram_tdp
    generic map(
        G_ADDR  => C_W_TIDX,
        G_DATA  => C_W_PHASE+1
    )
    port map(
        pi_clk_a    => clk,
        pi_en_a     => pi_table_offs_en,
        pi_we_a     => pi_table_offs_we,
        pi_addr_a   => pi_table_offs_addr,
        pi_data_a   => pi_table_offs_data,
        po_data_a   => po_table_offs_data,
        pi_clk_b    => clk,
        pi_en_b     => '1',
        pi_we_b     => '0',
        pi_addr_b   => std_logic_vector(tidx),
        pi_data_b   => (others => '0'),
        po_data_b   => table_offs_data
    );
    -- Unpack offs data
    po_phase_reset  <= table_offs_data(C_W_PHASE);
    po_phase_offset <= table_offs_data(C_W_PHASE-1 downto 0);

    ------------------------
    -- PIPELINE DELAY REG --
    ------------------------
    p_pipe_delay:process(clk, rstn)
    begin
        if rstn = '0' then
            r_phase_tidx    <= (others => '0');
            r_phase_valid   <= '0';
        elsif rising_edge(clk) then
            r_phase_tidx    <= tidx;
            r_phase_valid   <= tidx_valid;
        end if;
    end process;

end architecture rtl;

